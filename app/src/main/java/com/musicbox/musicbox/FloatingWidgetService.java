package com.musicbox.musicbox;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

public class FloatingWidgetService extends Service {

	private WindowManager mWinManager;
	private ImageView mWidgetHead;

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mWinManager = (WindowManager) getSystemService(WINDOW_SERVICE);
		mWidgetHead = new ImageView(this);
		mWidgetHead.setImageResource(R.drawable.widgethead);
		final int iconWidth = ContextCompat.getDrawable(getApplicationContext(), R.drawable.widgethead).getIntrinsicWidth();

		final WindowManager.LayoutParams winParams = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_PHONE,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
				PixelFormat.TRANSLUCENT);

		winParams.gravity = Gravity.TOP | Gravity.LEFT;
		winParams.x = -iconWidth/2;
		winParams.y = 100;

		mWidgetHead.setOnTouchListener(new View.OnTouchListener() {
			private int initialX;
			private int initialY;
			private float initialTouchX;
			private float initialTouchY;
			private int width;
			private int DEFAULT_START = -iconWidth/2;
			private int DEFAULT_END = width+iconWidth/2;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				//get width of drawable icon
				Display screen = mWinManager.getDefaultDisplay();
				Point size = new Point();
				screen.getSize(size);
				width = size.x;

				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						initialX = winParams.x;
						initialY = winParams.y;
						initialTouchX = event.getRawX();
						initialTouchY = event.getRawY();
						return true;
					case MotionEvent.ACTION_UP:
						//TODO: add animation
						if (winParams.x <= (width / 2)) {
							winParams.x = -(iconWidth/2);
						} else {
							winParams.x = width-(iconWidth/2);
						}

						mWinManager.updateViewLayout(mWidgetHead, winParams);
						return true;
					case MotionEvent.ACTION_MOVE:
						winParams.x = initialX + (int) (event.getRawX() - initialTouchX);
						winParams.y = initialY + (int) (event.getRawY() - initialTouchY);
						mWinManager.updateViewLayout(mWidgetHead, winParams);
						return true;
				}
				return false;
			}
		});

		mWinManager.addView(mWidgetHead, winParams);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mWidgetHead != null) {
			mWinManager.removeView(mWidgetHead);
		}
	}
}
