package com.musicbox.musicbox;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends NavigationToolbarActivity implements NavigationDrawerFragment.OnListItemClickedListener {
	private SharedPreferences mainPref;

	//preference keys
	private static final String ENABLE_APP = "ENABLE_APP";
	private static final String ENABLE_AUTO_UNLOCK = "ENABLE_AUTO_UNLOCK";
	private static final String ENABLE_ANIMATION = "ENABLE_ANIMATION";
	private static final String ENABLE_VIBRATION = "ENABLE_VIBRATION";
	private static final String APP_SIZE = "APP_SIZE";

	//preference entries/elements
	private CheckBox appEnabled, appearUnlock, animationEnabled, vibrationEnabled;
	private SeekBar appSize;
	private TextView appSizeStatus;

	@Override
	protected int getLayoutResource() {
		return R.layout.app_preference;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		//initialize elements on page
		appEnabled = (CheckBox) findViewById(R.id.pref_check_musicbox_enable);
		appearUnlock = (CheckBox) findViewById(R.id.pref_check_auto_unlock);
		animationEnabled = (CheckBox) findViewById(R.id.pref_check_animation);
		vibrationEnabled = (CheckBox) findViewById(R.id.pref_check_vibration);
		appSize = (SeekBar) findViewById(R.id.pref_seekbar_size);
		appSizeStatus = (TextView) findViewById(R.id.app_size_status);

		//configure initial values for seekbar (app size)
		appSize.setMax(200);

		//set values from preference
		mainPref = getPreferences(MODE_PRIVATE);
		appEnabled.setChecked(mainPref.getBoolean(ENABLE_APP, true));
		appearUnlock.setChecked(mainPref.getBoolean(ENABLE_AUTO_UNLOCK, false));
		animationEnabled.setChecked(mainPref.getBoolean(ENABLE_ANIMATION, true));
		vibrationEnabled.setChecked(mainPref.getBoolean(ENABLE_VIBRATION, false));
		appSize.setProgress(mainPref.getInt(APP_SIZE, 100));
		appSizeStatus.setText(String.valueOf(appSize.getProgress()));

		//seekbar listener for changing floating widget size
		appSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				appSizeStatus.setText(String.valueOf(appSize.getProgress()));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mainPref = getPreferences(MODE_APPEND);
		SharedPreferences.Editor editor = mainPref.edit();

		//save settings
		editor.putBoolean(ENABLE_APP, appEnabled.isChecked());
		editor.putBoolean(ENABLE_AUTO_UNLOCK, appearUnlock.isChecked());
		editor.putBoolean(ENABLE_ANIMATION, animationEnabled.isChecked());
		editor.putBoolean(ENABLE_VIBRATION, vibrationEnabled.isChecked());
		editor.putInt(APP_SIZE, appSize.getProgress());
		editor.apply();
	}

	public static void launch(NavigationToolbarActivity activity, View transitionView) {
		Intent openPreference = new Intent(activity, SettingsActivity.class);
		ActivityCompat.startActivity(activity, openPreference, null);
	}

	@Override
	public void openPreference() {
		mDrawerLayout.closeDrawer(GravityCompat.START);
	}
}
