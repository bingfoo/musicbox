package com.musicbox.musicbox;

import android.app.Activity;
import android.os.Environment;
import android.util.TypedValue;

import java.io.File;

public class SharedValue {
	public static final File MPATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);

	/**
	 * Converts dp to pixel approximately
	 * @param dp The value in dp
	 * @return The value in pixels
	 */
	public static int dp2px(int dp, Activity context) {
		return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics()));
	}
}
