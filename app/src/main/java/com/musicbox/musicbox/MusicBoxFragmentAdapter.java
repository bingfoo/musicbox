package com.musicbox.musicbox;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MusicBoxFragmentAdapter extends FragmentPagerAdapter {
	private String[] tab_title;
	private Context context;

	public MusicBoxFragmentAdapter(FragmentManager fm, Context context) {
		super(fm);
		this.context = context;
		tab_title = context.getResources().getStringArray(R.array.tabs_main);
	}

	@Override
	public Fragment getItem(int position) {
		return PageFragment.newInstance(position + 1);
	}

	@Override
	public int getCount() {
		return tab_title.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tab_title[position];
	}
}
