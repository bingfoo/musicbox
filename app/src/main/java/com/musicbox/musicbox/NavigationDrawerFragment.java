package com.musicbox.musicbox;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.musicbox.musicbox.recycler.NavListAdapter;
import com.musicbox.musicbox.recycler.NavListRow;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass for navigation drawer
 */
public class NavigationDrawerFragment extends Fragment {
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private boolean mFromSavedInstanceState;
    private View navView;

	private OnListItemClickedListener mCallback;
	private RecyclerView navList;
	private NavListAdapter navListAdapter;


    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

	//interface for managing this fragment in main activity
	public interface OnListItemClickedListener {
		void openPreference();
	}

	/**
	 * Associate callback to main activity to variable
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnListItemClickedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " have not implement listener interface");
		}
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
	    View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
	    navList = (RecyclerView) layout.findViewById(R.id.nav_items);
	    navListAdapter = new NavListAdapter(getActivity(), getNavData());
	    navList.setAdapter(navListAdapter);
	    navList.setLayoutManager(new LinearLayoutManager(getActivity()));
	    navList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				switch (position) {
					case 4:
						mCallback.openPreference();
						break;
				}
			}
		}));
	    return layout;
    }

	private List<NavListRow> getNavData() {
		List<NavListRow> data = new ArrayList<>();
		int[] iconIds = {R.drawable.new_playlist_selected, R.drawable.favorite_selected, R.drawable.purchase_unselected,
				R.drawable.feed_back_unselected, R.drawable.setting_unselected};
		String[] titles = getResources().getStringArray(R.array.nav_list);

		for (int i = 0; i < iconIds.length; i++) {
			NavListRow it = new NavListRow();
			it.iconId = iconIds[i];
			it.rowText = titles[i];
			data.add(it);
		}
		return data;
	}

    public void setUp(int fragmentId, DrawerLayout drawerLayout, android.support.v7.widget.Toolbar toolbar) {
        navView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        if (!mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(navView);
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }
}
