package com.musicbox.musicbox;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;

public class PageFragment extends Fragment {
	public static final String PAGE_NUM = "PAGE_NUM";
	private int mPage;
	private OnTabSelectedListener mCallback;

	public static PageFragment newInstance(int page) {
		Bundle args = new Bundle();
		args.putInt(PAGE_NUM, page);
		PageFragment fragment = new PageFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPage = getArguments().getInt(PAGE_NUM);
	}

	//interface for talking with MainActivity
	public interface OnTabSelectedListener {
		void startTrack(File trackFile, boolean resume);
		void openRightDrawer(File song);
		void setWidgetEnabled(boolean enabled);
		void showPerkPortal();
		void perkEventTriggered();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (OnTabSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " have not implement listener interface");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.view_in_tab, container, false);
		LinearLayout rootLayout = (LinearLayout) view.findViewById(R.id.tab_content);
		rootLayout.setGravity(Gravity.CENTER_HORIZONTAL);
		TextView textContent = (TextView) view.findViewById(R.id.tab_textview);

		switch (mPage) {
			case 1:
				//playlist
				Button chatheadShow = new Button(container.getContext());
				Button chatheadHide = new Button(container.getContext());
				Button showPerkPortal = new Button(container.getContext());
				Button eventBtn = new Button(container.getContext());

				chatheadShow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				chatheadShow.setText("Show widget");

				chatheadHide.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				chatheadHide.setText("Hide widget");

				showPerkPortal.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				showPerkPortal.setText("Show Portal");

				eventBtn.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				eventBtn.setText("Event Button");

				chatheadShow.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mCallback.setWidgetEnabled(true);
					}
				});

				chatheadHide.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mCallback.setWidgetEnabled(false);
					}
				});

				showPerkPortal.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mCallback.showPerkPortal();
					}
				});

				eventBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mCallback.perkEventTriggered();
					}
				});

				//add both buttons to root view
				rootLayout.addView(chatheadShow);
				rootLayout.addView(chatheadHide);
				rootLayout.addView(showPerkPortal);
				rootLayout.addView(eventBtn);
				break;
			case 2:
				//song
				if (SharedValue.MPATH.listFiles().length > 0) {
					textContent.setText("Found " + SharedValue.MPATH.list().length + " song(s)");
					final MediaMetadataRetriever retriever = new MediaMetadataRetriever();

					//add separator
					View hsep = new View(container.getContext());
					LinearLayout.LayoutParams sepParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp2px(1));
					sepParams.setMargins(0, 0, 0, 0);
					hsep.setBackgroundColor(getResources().getColor(R.color.separator_dark));
					rootLayout.addView(hsep, sepParams);            //add top separator

					for (final File file : SharedValue.MPATH.listFiles()) {
						retriever.setDataSource(file.getAbsolutePath());
						String songTitle = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
						String songArtist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);

						final RelativeLayout songEntry = new RelativeLayout(container.getContext());
						final ImageView songLayoutArt = new ImageView(container.getContext());
						songLayoutArt.setId(songLayoutArt.hashCode());
						final TextView songLayoutTitle = new TextView(container.getContext());
						songLayoutTitle.setId(songLayoutTitle.hashCode());
						final TextView songLayoutArtist = new TextView(container.getContext());
						final ImageView iconSlide = new ImageView(container.getContext());

						int pxfrom16dp = dp2px(16);

						//set overall layout for an entry
						songEntry.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
						songEntry.setPadding(0, 0, 0, 0);

						//set song album art
						RelativeLayout.LayoutParams artParams = new RelativeLayout.LayoutParams(dp2px(64), dp2px(64));
						artParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						artParams.addRule(RelativeLayout.CENTER_VERTICAL);
						artParams.setMargins(0, 0, pxfrom16dp, 0);
						byte[] art = retriever.getEmbeddedPicture();
						if (art != null) {
							Bitmap albumart = BitmapFactory.decodeByteArray(art, 0, art.length);
							songLayoutArt.setImageBitmap(albumart);
						}

						//set layout for title and artist
						LinearLayout songTitleArtist = new LinearLayout(container.getContext());
						RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, dp2px(64));
						songTitleArtist.setOrientation(LinearLayout.VERTICAL);
						textParams.addRule(RelativeLayout.RIGHT_OF, songLayoutArt.getId());
						songTitleArtist.setGravity(Gravity.CENTER_VERTICAL);

						//set song title
						LinearLayout.LayoutParams titleParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						songLayoutTitle.setText(songTitle);
						songLayoutTitle.setTextSize(16);
						songLayoutTitle.setTypeface(Typeface.DEFAULT_BOLD);

						//set song artist
						LinearLayout.LayoutParams artistParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						songLayoutArtist.setText(songArtist);
						songLayoutArtist.setTextSize(12);

						songTitleArtist.addView(songLayoutTitle, titleParams);
						songTitleArtist.addView(songLayoutArtist, artistParams);

						//set icon for right drawer
						RelativeLayout.LayoutParams iconParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
						iconParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						iconParams.addRule(RelativeLayout.CENTER_VERTICAL);
						iconParams.setMargins(dp2px(16), 0, dp2px(16), 0);
						iconSlide.setImageDrawable(getResources().getDrawable(R.drawable.chevron));

						iconSlide.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								mCallback.openRightDrawer(file);
							}
						});

						//add to layout an entry
						songEntry.addView(songLayoutArt, artParams);
						songEntry.addView(songTitleArtist, textParams);
						songEntry.addView(iconSlide, iconParams);

						songEntry.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								Handler handler = new Handler();
								songEntry.setBackgroundColor(Color.GRAY);
								handler.postDelayed(new Runnable() {
									@Override
									public void run() {
										songEntry.setBackgroundColor(getResources().getColor(R.color.background_material_light));
									}
								}, 400);
								mCallback.startTrack(file, false);
							}
						});


						//add to root view
						rootLayout.addView(songEntry);          //add song entry
						View sep = new View(container.getContext());
						sep.setBackgroundColor(getResources().getColor(R.color.separator_dark));
						rootLayout.addView(sep, sepParams);
					}
				} else {
					textContent.setText("No song in library");
				}
				break;
			case 3:
				//artist
				textContent.setText("Artist list");
		}
		return view;
	}

	//convert from dp to px
	private int dp2px(int dp) {
		return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics()));
	}
}
